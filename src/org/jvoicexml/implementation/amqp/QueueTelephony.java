/*
 * File:    $HeadURL: http://svn.code.sf.net/p/jvoicexml/code/tags/0.7.6.GA/org.jvoicexml.implementation.text/src/org/jvoicexml/implementation/text/TextTelephony.java $
 * Version: $LastChangedRevision: 4011 $
 * Date:    $Date: 2013-12-02 11:13:04 +0100 (Mo, 02. Dez 2013) $
 * Author:  $LastChangedBy: schnelle $
 *
 * JVoiceXML - A free VoiceXML implementation.
 *
 * Copyright (C) 2008-2015 JVoiceXML group - http://jvoicexml.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.jvoicexml.implementation.amqp;

import org.jvoicexml.implementation.amqp.text.TextSpokenInput;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

import javax.sound.sampled.AudioFormat;

import org.apache.log4j.Logger;

import org.jvoicexml.CallControlProperties;
import org.jvoicexml.ConnectionInformation;
import org.jvoicexml.SpeakableText;
import org.jvoicexml.client.text.TextConnectionInformation;
import org.jvoicexml.event.error.NoresourceError;
import org.jvoicexml.implementation.SpokenInput;
import org.jvoicexml.implementation.SynthesizedOutput;
import org.jvoicexml.implementation.Telephony;
import org.jvoicexml.implementation.TelephonyEvent;
import org.jvoicexml.implementation.TelephonyListener;
import org.jvoicexml.implementation.amqp.text.TextSynthesizedOutput;

/**
 * AMQP based implementation of {@link Telephony}.
 * 
 * @author Raphael Groner
 */
public final class QueueTelephony implements Telephony {
    /** Logger for this class. */
    private static final Logger LOGGER = Logger
            .getLogger(QueueTelephony.class);
    
    /** The name for the queue. */
    private final String queueName;
    
    /** The connection to the client. */
    private Connection connection;
    
    /** The channel to the client. */
    private Channel channel;
    
    /** The current text synthesizer. */
    private TextSynthesizedOutput textOutput;

    /** Registered call control listeners. */
    private final Collection<TelephonyListener> listener;

    /**
     * Constructs a new object.
     * @param nameQueue the name for the queue
     * @throws java.net.UnknownHostException
     */
    public QueueTelephony(final String nameQueue)
            throws UnknownHostException {
        listener = new java.util.ArrayList<>();
        
        //TODO config host
        final InetAddress host = InetAddress.getLocalHost();
        if (nameQueue == null || nameQueue.isEmpty()) {
            queueName = host.toString()+"/JVoiceXML";
        } else {
            queueName = nameQueue;
        }
    }
    
    /**
     * Get the name of the used queue.
     * @return the queue name
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * {@inheritDoc}
     * @throws org.jvoicexml.event.error.NoresourceError
     * @throws java.io.IOException
     */
    @Override
    public void play(final SynthesizedOutput output,
            final CallControlProperties props)
            throws NoresourceError, IOException {
        if (connection == null) {
            throw new NoresourceError("connection disconnected");
        }
        if (!(output instanceof TextSynthesizedOutput)) {
            throw new IOException("output does not deliver text!");
        }
        textOutput = (TextSynthesizedOutput) output;

        final Thread thread;
        thread = new Thread() {
            @Override
            public void run() {
                final String text = textOutput.getNextText().toString();
                firePlayStarted();
                byte[] bytes = text.getBytes();
                if (LOGGER.isDebugEnabled()) {
                    String message = "Play '.'";
                    message = message.replaceFirst(".", text);
                    LOGGER.debug(message, null);
                }
                try {
                    if (channel != null && channel.isOpen()) {
                        channel.basicPublish("", queueName, null, bytes);
                    }
                } catch (IOException ex) {
                    String message = "Can not publish but received '.'";
                    message = message.replaceFirst(".", text);
                    LOGGER.warn(message, ex);
                }
            }
        };
        thread.start();
    }

    /**
     * {@inherited}
     * @throws org.jvoicexml.event.error.NoresourceError
     */
    @Override
    public void stopPlay() throws NoresourceError {
        firePlayStopped();
    }

    /**
     * Notifies all listeners that play has started.
     */
    private void firePlayStarted() {
        final TelephonyEvent event =
            new TelephonyEvent(this, TelephonyEvent.PLAY_STARTED);
        fireTelephonyEvent(event);
    }

    /**
     * Notifies all listeners that play has stopped.
     */
    private void firePlayStopped() {
        final TelephonyEvent event =
            new TelephonyEvent(this, TelephonyEvent.PLAY_STOPPED);
        fireTelephonyEvent(event);
    }

    /**
     * {@inheritDoc}
     * @throws org.jvoicexml.event.error.NoresourceError
     * @throws java.io.IOException
     */
    @Override
    public void record(final SpokenInput input,
            final CallControlProperties props)
            throws NoresourceError, IOException {
        if (connection == null) {
            throw new NoresourceError("connection disconnected");
        }
        if (!(input instanceof TextSpokenInput)) {
            throw new IOException("input does not support texts!");
        }
        fireRecordStarted();
        final TextSpokenInput textInput = (TextSpokenInput) input;
        if (channel != null && channel.isOpen()) {
            String result = channel.basicConsume(queueName, null);
            textInput.notifyRecognitionResult(result);
        }
    }

    /**
     * Notification of the sender thread that the data has been transferred.
     */
    void recordStopped() {
        fireRecordStopped();
    }

    /**
     * {@inheritDoc}
     *
     * @return <code>null</code> since we do not support audio recordings.
     */
    @Override
    public AudioFormat getRecordingAudioFormat() {
        return null;
    }

    /**
     * {@inheritDoc}
     * @throws org.jvoicexml.event.error.NoresourceError
     * @throws java.io.IOException
     */
    @Override
    public void startRecording(final SpokenInput input,
            final OutputStream stream, final CallControlProperties props)
            throws NoresourceError, IOException {
        throw new NoresourceError(
                "recording to output streams is currently not supported");
    }

    /**
     * {@inheritDoc}
     * @throws org.jvoicexml.event.error.NoresourceError
     */
    @Override
    public void stopRecording() throws NoresourceError {
        fireRecordStopped();
    }

    /**
     * Notifies all listeners that play has started.
     */
    private void fireRecordStarted() {
        final TelephonyEvent event =
            new TelephonyEvent(this, TelephonyEvent.RECORD_STARTED);
        fireTelephonyEvent(event);
    }

    /**
     * Notifies all listeners that play has stopped.
     */
    private void fireRecordStopped() {
        final TelephonyEvent event =
            new TelephonyEvent(this, TelephonyEvent.RECORD_STOPPED);
        fireTelephonyEvent(event);
    }

    /**
     * Notifies all listeners about an unexpected disconnect.
     * @since 0.7
     */
    synchronized void fireHungup() {
        if (connection == null) {
            return;
        }
        connection = null;

        final TelephonyEvent event =
            new TelephonyEvent(this, TelephonyEvent.HUNGUP);
        fireTelephonyEvent(event);
    }

    /**
     * {@inheritDoc}
     * @throws org.jvoicexml.event.error.NoresourceError
     */
    @Override
    public void transfer(final String dest) throws NoresourceError {
        throw new NoresourceError("transfer is not supported!");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void activate() {
        listener.clear();
        connection = null;
        textOutput = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (IOException ex) {
                LOGGER.warn("Connection already closed.", ex);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getType() {
        return TextConnectionInformation.TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBusy() {
        return (channel != null && channel.isOpen());
    }

    /**
     * {@inheritDoc}
     * @throws org.jvoicexml.event.error.NoresourceError
     */
    @Override
    public void open() throws NoresourceError {
        if (connection != null) {
            try {
                channel = connection.createChannel();
            } catch (IOException ex) {
                LOGGER.warn("Channel creation failed.", ex);
                channel = null;
            }
        }
        if (channel == null) {
            throw new NoresourceError("No connection available.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void passivate() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("passivating...");
        }
        listener.clear();
        if (channel != null) {
            try {
                channel.close();
            } catch (TimeoutException ex) {
                // ignore
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(
                        QueueTelephony.class.getName()).log(
                                Level.SEVERE, null, ex);
            }
            channel = null;
        }

        connection = null;
        textOutput = null;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("...passivated");
        }
    }

    /**
     * {@inheritDoc}
     * @param info
     * @throws java.io.IOException
     */
    @Override
    public void connect(final ConnectionInformation info) throws IOException {
        if (info != null && info instanceof TextConnectionInformation) {
            TextConnectionInformation textInfo = (TextConnectionInformation)info;
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(getHostAddress(textInfo.getAddress()));
            try {
                connection = factory.newConnection();
            } catch (TimeoutException ex) {
                LOGGER.warn("Connection failed.", ex);
                throw new IOException(ex.getMessage());
            }
        } else {
            throw new IOException("Unsupported connection information '"
                    + info + "'");
        }
    }

    private String getHostAddress(InetAddress server) throws UnknownHostException {
        if (server == null) {
            return InetAddress.getLocalHost().getHostAddress();
        } else {
            return server.getHostAddress();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disconnect(final ConnectionInformation client) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("disconnecting from '" + client.getCallingDevice()
                + "'...");
        }

        try {
            if (channel != null && channel.isOpen()) {
                channel.abort();
            }
            if (connection != null) {
                connection.abort();
                connection.close();
                connection = null;
            }
        } catch (IOException e) {
            LOGGER.error("Connection is already closed.", e);
        }
        connection = null;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("disconnected");
        }
    }

    /**
     * {@inheritDoc}
     * @param callListener
     */
    @Override
    public void addListener(final TelephonyListener callListener) {
        synchronized (listener) {
            listener.add(callListener);
        }
    }

    /**
     * {@inheritDoc}
     * @param callListener
     */
    @Override
    public void removeListener(
            final TelephonyListener callListener) {
            synchronized (listener) {
                listener.remove(callListener);
            }
    }

    /**
     * Notifies all registered listeners about the given event.
     * @param event the event.
     */
    private void fireTelephonyEvent(final TelephonyEvent event) {
        synchronized (listener) {
            final Collection<TelephonyListener> copy =
                new java.util.ArrayList<>();
            copy.addAll(listener);
            for (TelephonyListener current : copy) {
                switch(event.getEvent()) {
                case TelephonyEvent.ANSWERED:
                    current.telephonyCallAnswered(event);
                    break;
                case TelephonyEvent.HUNGUP:
                    current.telephonyCallHungup(event);
                default:
                    current.telephonyMediaEvent(event);
                }
            }
        }
    }

    @Override
    public boolean isActive() {
        return (channel != null && channel.isOpen());
    }

    @Override
    public void hangup() {
        if (channel != null) {
            try {
                channel.abort();
                channel.close();
                channel = null;
            } catch (IOException | TimeoutException ex) {
                LOGGER.error("Hangup failed.", ex);
            }
        }
    }
}
