/*
 * File:    $HeadURL: http://svn.code.sf.net/p/jvoicexml/code/tags/0.7.6.GA/org.jvoicexml.implementation.text/src/org/jvoicexml/implementation/text/TextPlatformFactory.java $
 * Version: $LastChangedRevision: 2583 $
 * Date:    $LastChangedDate: 2011-02-16 17:02:39 +0100 (Mi, 16. Feb 2011) $
 * Author:  $LastChangedBy: schnelle $
 *
 * JVoiceXML - A free VoiceXML implementation.
 *
 * Copyright (C) 2008-2015 JVoiceXML group - http://jvoicexml.sourceforge.net
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.jvoicexml.implementation.amqp;

import org.jvoicexml.implementation.PlatformFactory;
import org.jvoicexml.implementation.ResourceFactory;
import org.jvoicexml.implementation.SpokenInput;
import org.jvoicexml.implementation.SynthesizedOutput;
import org.jvoicexml.implementation.Telephony;
import org.jvoicexml.implementation.amqp.text.TextSpokenInputFactory;
import org.jvoicexml.implementation.amqp.text.TextSynthesizedOutputFactory;

/**
 * Platform factory for the AMQP based implementation platform.
 *
 * @author Raphael Groner
 */
public final class QueuePlatformFactory implements PlatformFactory {
    /** Number of instances that the factories will create. */
    private int instances;

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceFactory<SpokenInput> getSpokeninput() {
        final TextSpokenInputFactory factory =
            new TextSpokenInputFactory();
        factory.setInstances(instances);
        return factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceFactory<SynthesizedOutput> getSynthesizedoutput() {
        final TextSynthesizedOutputFactory factory =
            new TextSynthesizedOutputFactory();
        factory.setInstances(instances);
        return factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceFactory<Telephony> getTelephony() {
        final QueueTelephonyFactory factory = new QueueTelephonyFactory();
        factory.setInstances(instances);
        return factory;
    }

    /**
     * Sets the number of instances that the factories will create.
     * @param number Number of instances to create.
     */
    public void setInstances(final int number) {
        instances = number;
    }
}
